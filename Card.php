<?php

class Card
{
  private $signature;
  private $code;

  const CODE = [ 
    ' ', ' ' , ' ', ' ',
    '2c', '2d', '2h', '2s',
    '3c', '3d', '3h', '3s',
    '4c', '4d', '4h', '4s',
    '5c', '5d', '5h', '5s',
    '6c', '6d', '6h', '6s',
    '7c', '7d', '7h', '7s',
    '8c', '8d', '8h', '8s',
    '9c', '9d', '9h', '9s',
    'Tc', 'Td', 'Th', 'Ts',
    'Jc', 'Jd', 'Jh', 'Js',
    'Qc', 'Qd', 'Qh', 'Qs',
    'Kc', 'Kd', 'Kh', 'Ks',
    'Ac', 'Ad', 'Ah', 'As',
  ];
  public function __construct(string $signature, Deck $deck)
  {
    if (!preg_match('/^([23456789TJQKA][cdhs])$/', $signature)) {
        throw new \InvalidArgumentException(
            sprintf('Argument "$signature" must be correct, actual value: "%s"', $signature)
        );
    }
    
    $code = self::generateCode($signature);
    $deck->checkCardAvailable($code);
    $this->signature = $signature;
    $this->code = $code;


  }

  public static function generateCode(string $signature)
  {
    $code = 0;
    switch ($signature[0]) {
      case '2':
        $code = 4;
        break;
      case '3':
        $code = 8;
        break;
      case '4':
        $code = 12;
        break;
      case '5':
        $code = 16;
        break;
      case '6':
        $code = 20;
        break;
      case '7':
        $code = 24;
        break;
      case '8':
        $code = 28;
        break;
      case '9':
        $code = 32;
        break;
      case 'T':
        $code = 36;
        break;
      case 'J':
        $code = 40;
        break;
      case 'Q':
        $code = 44;
        break;
      case 'K':
        $code = 48;
        break;
      case 'A':
        $code = 52;
        break;
    }
    switch ($signature[1]) {
      case 'c':
        $code += 0;
        break;
      case 'd':
        $code += 1;
        break;
      case 'h':
        $code += 2;
        break;
      case 's':
        $code += 3;
        break;
    }
    return $code;
  }

  public static function getSignatureFromCode($code)
  {
    return self::CODE[$code];
  }

  public function getSignature()
  {
    return $this->signature;
  }

  public function getCode()
  {
    return $this->code;
  }

  public static function generateDeck()
  {
    $arr = [];
    for ($i=4; $i < 56; $i++) { 
      $arr[$i] = '';
    }
    return $arr;
  }

}