<?php

class Transform
{

  public static function replaceTen(string $str) {
    return preg_replace('/10/', 'T', $str);
  }

  public static function resumeTen(string $str) {
    return preg_replace('/T/', '10', $str);
  }
}