<?php

class CommandLine
{
  public static function parseOptions($args, $allowed = [])
  {
    $options = [];
    $count = count($args);

    // retrive arguments and populate $options array
    for($i = 1; $i < $count; $i++) {
      // retrieve arguments in form of --abc=foo
      if (preg_match('/^--([-A-Z0-9]+)=(.+)$/i', $args[$i], $matches)) {
        if (empty($allowed) || in_array($matches[1], $allowed)) {

          if (array_key_exists($matches[1], $options)) {
            throw new Exception('Duplicate option ' . $matches[1]);
          }
          $options[$matches[1]] = $matches[2];
        } else {
          throw new Exception('Unrecognized option ' . $matches[1]);
        }
      }
      // invalid option format
      else {
        throw new Exception('Invalid option format at ' . $args[$i]);
      }
    }
 
    return $options;
  }
}