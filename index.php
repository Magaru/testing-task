#!/usr/bin/env php
<?php
if (php_sapi_name() != 'cli') {
  die('Must run from command line');
}

require 'Card.php';
require 'Combinatorics.php';
require 'CommandLine.php';
require 'Deck.php';
require 'Evaluation.php';
require 'Player.php';
require 'Transform.php';
require 'Validator.php';

try {
  // Set allowed params from CLI
  $allowed = ['board', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10'];

  // Parse params
  $params = CommandLine::parseOptions($argv, $allowed);

  // Init new deck
  $deck = new Deck(Card::generateDeck());

  // Init board's cards
  if (!isset($params['board'])) {
    throw new Exception("Missed board option");
  }

  $board = Validator::inputChar($params['board'], 'board');
  $board = Transform::replaceTen($board);
  $board = Validator::checkBoardCards($board);
  $boardCards = str_split($board, 2);

  $board = [];
  foreach ($boardCards as $boardCard) {
    $board[] = new Card($boardCard, $deck);
  }
  // END init board

  unset($params['board']); // Delete board from array with PARAMS

  // Init Player's cards
  if (count($params) < 2) {
   throw new Exception("Players must be between 2 and 10"); 
  }

  // Create array of players
  $players = [];
  foreach ($params as $key => $param) {
    $card = Validator::inputChar($param, $key);
    $card = Transform::replaceTen($card);
    $card = Validator::checkPocketCards($card, $key);
    $pocketCards = str_split($card, 2);
    $firstCard = new Card($pocketCards[0], $deck);
    $secondCard = new Card($pocketCards[1], $deck);
    $player = new Player($key, $firstCard, $secondCard);

    //Init HandValue (besthand)
    $player->evalHandWithBoard($board);

    $players[] = $player;
  }
  //=================================

  // Custom sorting player By HandPower
  usort($players, function($a, $b)
  {
    if ($a->getHandPower() === $b->getHandPower()) {
      if ($a->getHandPower() === Evaluation::HIGH_CARD) {
        if ($a->getComboCards() === Evaluation::compareHighCard($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::ONE_PAIR) {
        if ($a->getComboCards() === Evaluation::compareOnePair($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::TWO_PAIR) {
        if ($a->getComboCards() === Evaluation::compareTwoPair($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::THREE_OF_AKIND) {
        if ($a->getComboCards() === Evaluation::compareThreeOfAKind($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::STRAIGHT) {
        if ($a->getComboCards() === Evaluation::compareStraight($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::FLUSH) {
        if ($a->getComboCards() === Evaluation::compareFlush($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::FULL_HOUSE) {
        if ($a->getComboCards() === Evaluation::compareFullHouse($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::FOUR_OF_AKIND) {
        if ($a->getComboCards() === Evaluation::compareFourOfAKind($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }
      if ($a->getHandPower() === Evaluation::STRAIGHT_FLUSH) {
        if ($a->getComboCards() === Evaluation::compareStraight($a->getComboCards(), $b->getComboCards())) {
          return -1;
        }
        if ($a->getName() > $b->getName()) {
          return 1;
        }
        return 1;
      }

    }
    return strcmp($b->getHandPower(), $a->getHandPower());
  });
  // =================================
  // End sorting


  // OutPut player's hands
  foreach ($players as $player) {
    echo $player->getTransformFormatFullOutputInfo();
    echo PHP_EOL;
  }

} catch (Exception $e) {
  echo 'error: ' . $e->getMessage();
  return false;
}
