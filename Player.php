<?php

class Player
{
  private $name;
  private $cards = [];
  private $bestHandInfo;

  public function __construct(string $key, Card $firstCard, Card $secondCard)
  {
    $this->name = $key;
    $this->cards[] = $firstCard;
    $this->cards[] = $secondCard;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getCards()
  {
    return $this->cards;
  }

  public function evalHandWithBoard($board)
  {
    $arrayOfCard = array_merge($this->cards, $board);
    $combinatorics = new Math_Combinatorics;
    $cardsets = $combinatorics->combinations($arrayOfCard, 5);
    $eval = Evaluation::evalHand($cardsets);
    $this->bestHandInfo = $eval;
  }

  public function getHandPower()
  {
   return $this->bestHandInfo->getHandPower(); 
  }

  public function getHandPowerText()
  {
   return Evaluation::getText($this->bestHandInfo->getHandPower()); 
  }

  public function getComboCards()
  {
    return $this->bestHandInfo->getComboCards();
  }

  public function getComboCardsText()
  {
    $arr = $this->bestHandInfo->getComboCards();
    $res = '[';
    foreach ($arr as $obj) {
      $res .= $obj->getSignature();
      $res .= '';
    }
    $res .= ']';
    return $res;
  }

  public function getPocketCardsText()
  {
    $arr = $this->cards;
    $res = '[' . $arr[0]->getSignature() . ' ' . $arr[1]->getSignature() . ']';
    return $res;
  }

  public function getTransformFormatFullOutputInfo()
  {
    $pocket = [];
    $arrPocket = $this->cards;
    foreach ($arrPocket as $card) {
      $pocket[] = $card->getCode();
    }

    // Sorting Pocket cards if it's pair
    if ( intdiv($pocket[0], 4) === intdiv($pocket[1], 4) ) {
      sort($pocket);
    } else {
      rsort($pocket);
    }
    //==================================

    $combo = [];
    $arrCombo = $this->getComboCards();
    foreach ($arrCombo as $card) {
      $combo[] = $card->getCode();
    }
    sort($combo);

    //replace Ace if it Straight or Straight Flush
    if ( ( ($this->getHandPower() === Evaluation::STRAIGHT_FLUSH ) || ($this->getHandPower() === Evaluation::STRAIGHT ) ) && ( intdiv($combo[0], 4) === 1) ) {
      array_unshift($combo, array_pop($combo));
    }
    //============================================

    $outputPocketArray = array_intersect($pocket, $combo);

    foreach ($outputPocketArray as $key => $value) {
      $outputPocketArray[$key] = Card::getSignatureFromCode($value);
    }
    $pocketTxt = implode(' ', $outputPocketArray);
    $pocketTxt = Transform::resumeTen($pocketTxt);

    foreach ($combo as $key => $value) {
      $combo[$key] = Card::getSignatureFromCode($value);
    }
    $comboTxt = implode(' ', $combo);
    $comboTxt = Transform::resumeTen($comboTxt);

    $name = $this->name;
    $text = $this->getHandPowerText();

    $result = $name . ' ' . $text . ' [' . $comboTxt . '] [' . $pocketTxt . ']';

    return $result;
  }

}