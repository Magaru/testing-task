<?php

class Evaluation
{
  const ROYAL_FLUSH = 9;
  const STRAIGHT_FLUSH = 8;
  const FOUR_OF_AKIND = 7;
  const FULL_HOUSE = 6;
  const FLUSH = 5;
  const STRAIGHT = 4;
  const THREE_OF_AKIND = 3;
  const TWO_PAIR = 2;
  const ONE_PAIR = 1;
  const HIGH_CARD = 0;

  const TEXT = ['High card', 'One pair', 'Two pair', 'Three of a Kind', 'Straight', 'Flush', 'Full House', 'Four of a Kind', 'Straight Flush', 'Royal Flush'];

  private $handpower;
  private $combocards;

  public static function getText($val)
  {
    return self::TEXT[$val];
  }

  public function getComboCards()
  {
    return $this->combocards;
  }

  public function getHandPower()
  {
    return $this->handpower;
  }

  public static function evalHand(array $cardsets)
  {
    $hand = new self;
    foreach ($cardsets as $cardset) {

      if ( self::isFlushRoyal($cardset) ) {
        if ($hand->handpower < self::ROYAL_FLUSH) {
          $hand->handpower = self::ROYAL_FLUSH;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isStraightFlush($cardset) ) {
        if ($hand->handpower === self::STRAIGHT_FLUSH) {
          $besthand = self::compareStraight($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::STRAIGHT_FLUSH) {
          $hand->handpower = self::STRAIGHT_FLUSH;
          $hand->combocards = $cardset;
        }

      } elseif ( self::isFourOfAKind($cardset) ) {
        if ($hand->handpower === self::FOUR_OF_AKIND) {
          $besthand = self::compareFourOfAKind($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::FOUR_OF_AKIND) {
          $hand->handpower = self::FOUR_OF_AKIND;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isFullHouse($cardset) ) {
        if ($hand->handpower === self::FULL_HOUSE) {
          $besthand = self::compareFullHouse($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::FULL_HOUSE) {
          $hand->handpower = self::FULL_HOUSE;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isFlush($cardset) ) {
        if ($hand->handpower === self::FLUSH) {
          $besthand = self::compareFlush($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::FLUSH) {
          $hand->handpower = self::FLUSH;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isStraight($cardset) ) {
        if ($hand->handpower === self::STRAIGHT) {
          // $besthand = self::compareStraight($hand->combocards, $cardset);
          $besthand = self::compareStraightWithSuiteForOnePlayerOnly($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::STRAIGHT) {
          $hand->handpower = self::STRAIGHT;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isThreeOfAKind($cardset) ) {
        if ($hand->handpower === self::THREE_OF_AKIND) {
          $besthand = self::compareThreeOfAKind($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::THREE_OF_AKIND) {
          $hand->handpower = self::THREE_OF_AKIND;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isTwoPair($cardset) ) {
        if ($hand->handpower === self::TWO_PAIR) {
          $besthand = self::compareTwoPair($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::TWO_PAIR) {
          $hand->handpower = self::TWO_PAIR;
          $hand->combocards = $cardset;
        }
      } elseif ( self::isPair($cardset) ) {
        if ($hand->handpower === self::ONE_PAIR) {
          $besthand = self::compareOnePair($hand->combocards, $cardset);
          if ( $besthand ) {
            $hand->combocards = $besthand;
          }
        }
        if ($hand->handpower < self::ONE_PAIR) {
          $hand->handpower = self::ONE_PAIR;
          $hand->combocards = $cardset;
        }
      } elseif ( ($hand->combocards) && ($hand->handpower === self::HIGH_CARD) ) {
        $besthand = self::compareHighCard($hand->combocards, $cardset);
        if ( $besthand ) { $hand->combocards = $besthand; }
        $hand->handpower = self::HIGH_CARD;
      } elseif ( !($hand->combocards) ) {
        $hand->handpower = self::HIGH_CARD;
        $hand->combocards = $cardset;
      }
    }
    return $hand;
  }

  /*
   * Return Greater hand or false if hands is equivalent
   */
  public static function compareStraight(array $hand1, array $hand2)
  {
    $arr = [];
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] = intdiv($card->getCode(), 4);
      if ( intdiv($card->getCode(), 4) === 13 ) {
        $arr[0] = 0;
      }
    }
    sort($arr);
    if ( $arr[1] === 1 ) {
      unset($arr[5]);
    }
    $h1power = max($arr);

    $arr = [];
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] = intdiv($card->getCode(), 4);
      if ( intdiv($card->getCode(), 4) === 13 ) {
        $arr[0] = 0;
      }
    }
    sort($arr);
    if ( $arr[1] === 1 ) {
      unset($arr[5]);
    }

    $h2power = max($arr);

    if ($h1power > $h2power) {
      return $hand1;
    }
    if ($h1power < $h2power) {
      return $hand2;
    }

    return false;
  }

  public static function compareStraightWithSuiteForOnePlayerOnly(array $hand1, array $hand2)
  {
    $arr = [];
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] = intdiv($card->getCode(), 4);
      if ( intdiv($card->getCode(), 4) === 13 ) {
        $arr[0] = 0;
      }
    }
    sort($arr);
    if ( $arr[1] === 1 ) {
      unset($arr[5]);
    }
    $h1power = max($arr);

    $arr = [];
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] = intdiv($card->getCode(), 4);
      if ( intdiv($card->getCode(), 4) === 13 ) {
        $arr[0] = 0;
      }
    }
    sort($arr);
    if ( $arr[1] === 1 ) {
      unset($arr[5]);
    }

    $h2power = max($arr);

    if ($h1power > $h2power) {
      return $hand1;
    }
    if ($h1power < $h2power) {
      return $hand2;
    }

// Straight variant with pocket pair
    if ($h1power === $h2power) {

      $arr1 = [];
      foreach ($hand1 as $card) {
        $arr1[($card->getCode())] = $card->getCode();
      }
      sort($arr1);

      $arr2 = [];
      foreach ($hand2 as $card) {
        $arr2[($card->getCode())] = $card->getCode();
      }
      sort($arr2);

// Return hand with lower CardCode
      for ($i=4; $i > -1 ; $i--) { 
        if ( $arr1[$i] < $arr2[$i] ) {
          return $hand1;
        }
        if ( $arr1[$i] > $arr2[$i] ) {
          return $hand2;
        }
      }

// =================================
      var_dump($arr1);
      var_dump($arr2);

    }

    return false;
  }

  public static function compareFourOfAKind(array $hand1, array $hand2)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h1power = array_search ('4', $arr);
    $h1kick = array_search ('1', $arr);

    $arr = array_fill(1, 13, 0);
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h2power = array_search ('4', $arr);
    $h2kick = array_search ('1', $arr);

    if ($h1power > $h2power) {
      return $hand1;
    }
    if ($h1power < $h2power) {
      return $hand2;
    }
    if ($h1kick > $h2kick) {
      return $hand1;
    }
    if ($h1kick < $h2kick) {
      return $hand2;
    }
    return false;
  }

  public static function compareFullHouse(array $hand1, array $hand2)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h1power = array_search ('3', $arr);
    $h1kick = array_search ('2', $arr);

    $arr = array_fill(1, 13, 0);
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h2power = array_search ('3', $arr);
    $h2kick = array_search ('2', $arr);

    if ($h1power > $h2power) {
      return $hand1;
    }
    if ($h1power < $h2power) {
      return $hand2;
    }
    if ($h1kick > $h2kick) {
      return $hand1;
    }
    if ($h1kick < $h2kick) {
      return $hand2;
    }
    return false;    
  }

  public static function compareFlush(array $hand1, array $hand2)
  {

    $arr1 = [];
    foreach ($hand1 as $card) {
      $arr1[] = intdiv($card->getCode(), 4);
    }
    sort($arr1);

    $arr2 = [];
    foreach ($hand2 as $card) {
      $arr2[] = intdiv($card->getCode(), 4);
    }
    sort($arr2);

    for ($i = 4; $i > -1 ; $i--) { 
      if ( $arr1[$i] > $arr2[$i] ) {
        return $hand1;
      }
      if ( $arr1[$i] < $arr2[$i] ) {
        return $hand2;
      }
    }
    return false;

  }

  public static function compareThreeOfAKind(array $hand1, array $hand2)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h1power = array_search ('3', $arr);
    $h1kick = array_keys ($arr, '1');

    $arr = array_fill(1, 13, 0);
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h2power = array_search ('3', $arr);
    $h2kick = array_keys ($arr, '1');

    if ($h1power > $h2power) {
      return $hand1;
    }
    if ($h1power < $h2power) {
      return $hand2;
    }
    if ($h1kick[0] > $h2kick[0]) {
      return $hand1;
    }
    if ($h1kick[0] < $h2kick[0]) {
      return $hand2;
    }
    if ($h1kick[1] > $h2kick[1]) {
      return $hand1;
    }
    if ($h1kick[1] < $h2kick[1]) {
      return $hand2;
    }
    return false;    
  }

  public static function compareTwoPair(array $hand1, array $hand2)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h1power = array_keys ($arr, '2');
    $h1kick = array_search ('1', $arr);

    $arr = array_fill(1, 13, 0);
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h2power = array_keys ($arr, '2');
    $h2kick = array_search ('1', $arr);

    if (max($h1power) > max($h2power)) {
      return $hand1;
    }
    if (max($h1power) < max($h2power)) {
      return $hand2;
    }
    if (min($h1power) > min($h2power)) {
      return $hand1;
    }
    if (min($h1power) < min($h2power)) {
      return $hand2;
    }
    if ($h1kick > $h2kick) {
      return $hand1;
    }
    if ($h1kick < $h2kick) {
      return $hand2;
    }
    return false;
  }

  public static function compareOnePair(array $hand1, array $hand2)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h1power = array_search ('2', $arr);
    $h1kick = array_keys ($arr, '1');

    $arr = array_fill(1, 13, 0);
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h2power = array_search ('2', $arr);
    $h2kick = array_keys ($arr, '1');

    if ( $h1power > $h2power ) {
      return $hand1;
    }
    if ( $h1power < $h2power ) {
      return $hand2;
    }

    for ($i = 2; $i > -1 ; $i--) { 
      if ($h1kick[$i] > $h2kick[$i]) {
        return $hand1;
      }
      if ($h1kick[$i] < $h2kick[$i]) {
        return $hand2;
      }
    }

    return false;

  }

  public static function compareHighCard(array $hand1, array $hand2)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($hand1 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h1kick = array_keys ($arr, '1');

    $arr = array_fill(1, 13, 0);
    foreach ($hand2 as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }
    $h2kick = array_keys ($arr, '1');

    for ($i = 4; $i > -1 ; $i--) { 
      if ($h1kick[$i] > $h2kick[$i]) {
        return $hand1;
      }
      if ($h1kick[$i] < $h2kick[$i]) {
        return $hand2;
      }
    }
    return false;
  }


  public static function isFlushRoyal($cardset)
  {
    $arr = [];
    foreach ($cardset as $card) {
      $arr[] = $card->getCode();
    }
    sort($arr);
    $rflashC = [36,40,44,48,52];
    $rflashD = [37,41,45,49,53];
    $rflashH = [38,42,46,50,54];
    $rflashS = [39,43,47,51,55];
    if ( $res = ( ($arr === $rflashC) || ($arr === $rflashD) || ($arr === $rflashH) || ($arr === $rflashS) ) ) {
      return $cardset;
    };
    return false;
  }

  public static function isStraightFlush($cardset)
  {

    if ( self::isFlush($cardset) && self::isStraight($cardset) ) {
      return self::isStraight($cardset);
    }
    return false;
  }

  public static function isFourOfAKind($cardset)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($cardset as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }

    foreach ($arr as $value) {
      if ($value === 4) {
        return $cardset;
      }
    }
    return false;
  }

  public static function isFlush($cardset)
  {
    $arr = [];

    foreach ($cardset as $card) {
      $arr[] = $card->getCode();
    }

    $newarr = array_map(function ($array_item){
        return $array_item % 4;
    }, $arr);
    if ( 
      ($newarr[0] === $newarr[1] )
      && ( $newarr[1] === $newarr[2] )
      && ( $newarr[2] === $newarr[3] )
      && ( $newarr[3] === $newarr[4] ) ) {

      return $cardset;
    }

    return false;
  }

  public static function isStraight($cardset)
  {
    $arr = array_fill(0, 13, 0);
    foreach ($cardset as $card) {
      $arr[intdiv($card->getCode(), 4)] = 1;

      if ( intdiv($card->getCode(), 4) === 13 ) {
        $arr[0] = 1;
      }
    }

    $count = 0;
    foreach ($arr as $value) {
      if ($value === 1) {
        $count += 1;
        if ($count === 5) {
          return $cardset;
        }
      } else {
        $count = 0;
      }
    }

    return false;
  }

  public static function isFullHouse($cardset)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($cardset as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }

    foreach ($arr as $value) {
      if ($value === 3) {
        foreach ($arr as $value) {
          if ($value === 2) {
            return $cardset;
          }
        }
      }
    }
    return false;
  }

  public static function isThreeOfAKind($cardset)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($cardset as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }

    foreach ($arr as $value) {
      if ($value === 3) {
        return $cardset;
      }
    }
    return false;
  }

  public static function isTwoPair($cardset)
  {
    $arr = array_fill(1, 13, 0);
    foreach ($cardset as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }

    foreach ($arr as $key => $value) {
      if ($value === 2) {
        unset($arr[$key]);
        foreach ($arr as $value) {
          if ($value === 2) {
            return $cardset;
          }
        }
      }
    }
    return false;
  }

  public static function isPair($cardset)
  {

    $arr = array_fill(1, 13, 0);
    foreach ($cardset as $card) {
      $arr[intdiv($card->getCode(), 4)] += 1;
    }

    foreach ($arr as $value) {
      if ($value === 2) {
        return $cardset;
      }
    }
    return false;
  }

}

