<?php

class Deck
{
  private $deck;

  public function __construct(array $deck)
  {
    $this->deck = $deck;
  }

  public function checkCardAvailable($code)
  {
    if (!array_key_exists($code, $this->deck)) {
      throw new Exception('Cards can\'t duplikate');
    }
    $this->removeCardFromDeck($code);
  }

  public function removeCardFromDeck($code)
  {
    unset($this->deck[$code]);
  }
}