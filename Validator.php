<?php

class Validator
{

  static function inputChar(string $str, string $name = '')
  {
    $allowedChar = '/^[JQKAcdhs0-9]+$/';
    if(!preg_match($allowedChar, $str)){
      throw new Exception('SyntaxError ' . $name);
    }
    return $str;
  }

  static function checkBoardCards(string $str)
  {
    $allowedChar = '/^([23456789TJQKA][cdhs]){3,5}+$/';
    if(!preg_match($allowedChar, $str)){
      throw new Exception('Invalid boardCards ');
    }
    return $str;
  }

  static function checkPocketCards(string $str, string $name = '')
  {
    $allowedChar = '/^([23456789TJQKA][cdhs]){2}+$/';
    if(!preg_match($allowedChar, $str)){
      throw new Exception('Invalid pocketCards ' . $name);
    }
    return $str;
  }
}